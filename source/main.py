from converter import NumConverter
from functions import ALPHABET


def run_check(in_ss):
    # Проверка вводных данных пользователем.
    if in_ss == 16:
        in_num = input('Введите число для перевода: ')
        try:
            for i in str.upper(in_num):
                if str(i) not in ALPHABET:
                    raise Exception
        except Exception:
            print('Значение каждого разряда числа должно быть меньше значения системы счисления. ')
            return False
        return in_num
    else:
        try:
            in_num = int(input('Введите число для перевода: '))
            if in_num < 0:
                raise Exception
            for i in str(in_num):
                if int(i) >= int(in_ss):
                    raise Exception
            return str(in_num)
        except Exception:
            print('Число должно быть: целое, больше 0, значение каждого разряда числа должно быть меньше значения'
                  ' системы счисления.')
            return False


def menu():
    # меню
    choise = -1
    while choise != 0:
        print(
            '\n'
            'Выберите нужный вариант перевода:\n'
            '1. 2 → 10\n'
            '2. 10 → 2\n'
            '3. 2 → 8\n'
            '4. 8 → 2\n'
            '5. 8 → 16\n'
            '6. 16 → 8\n'
            '0. Выйти'
        )

        # Проверяем ввод данных
        try:
            choise = int(input('Введите номер меню: '))
        except Exception:
            print('\nВведите целое число от 0 до 6.')
            continue

        if choise == 1:
            in_num = run_check(2)
            if in_num:
                convert = NumConverter(
                    in_ss='2',
                    out_ss='10',
                    in_number=in_num
                )
                print(convert)
        elif choise == 2:
            in_num = run_check(10)
            if in_num:
                convert = NumConverter(
                    in_ss='10',
                    out_ss='2',
                    in_number=in_num
                )
                print(convert)
        elif choise == 3:
            in_num = run_check(2)
            if in_num:
                convert = NumConverter(
                    in_ss='2',
                    out_ss='8',
                    in_number=in_num
                )
                print(convert)
        elif choise == 4:
            in_num = run_check(8)
            if in_num:
                convert = NumConverter(
                    in_ss='8',
                    out_ss='2',
                    in_number=in_num
                )
                print(convert)
        elif choise == 5:
            in_num = run_check(8)
            if in_num:
                convert = NumConverter(
                    in_ss='8',
                    out_ss='16',
                    in_number=in_num
                )
                print(convert)
        elif choise == 6:
            in_num = run_check(16)
            if in_num:
                convert = NumConverter(
                    in_ss='16',
                    out_ss='8',
                    in_number=in_num
                )
                print(convert)
        elif choise == 0:
            print('\nВыход')
        else:
            print('\nОшибка, введите число от 0 до 6.')


if __name__ == '__main__':
    menu()
